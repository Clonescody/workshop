const Stellar = require("stellar-sdk");
const Config = require("../config/config.js");
const Account = require("./account.js");
const Server = new Stellar.Server(Config.api_url);

Stellar.Network.useTestNetwork();

class StellarCore {
  constructor() {
    this.escrowObject = null;
    this.accounts = [];
  }

  async initAccount(name) {
    this.accounts.push(new Account(Server, name));
    await this.accounts[this.accounts.length - 1].initAccount();
  }

  getAccountByName(name) {
    for (let i = 0; i < this.accounts.length; i++) {
      if (this.accounts[i].getName() === name) {
        return this.accounts[i];
      }
    }
  }

  getEscrowAccount() {
    return this.escrowObject;
  }

  isAccountInitialized(name) {
    for (let i = 0; i < this.accounts.length; i++) {
      if (
        this.accounts[i].isAccountInitialized() &&
        this.accounts[i].getName() === name
      ) {
        return true;
      }
    }
    return false;
  }

  async sendTransaction(sender, amount, destination) {
    for (let i = 0; i < this.accounts.length; i++) {
      if (this.accounts[i].getName() === sender) {
        await this.accounts[i].sendTransaction(amount, destination);
        break;
      }
    }
  }

  async createThisShit(firstEntity, secondEntity, name, amount) {
    const res = await this.createEscrow(
      firstEntity,
      secondEntity,
      name,
      amount
    );
    return res;
  }

  async createEscrowAccount(name, amount) {
    try {
      const acc = new Account(Server, name);
      await acc.initEscrow(amount);
      this.accounts.push(acc);
      return acc;
    } catch (error) {
      throw error;
    }
  }

  async createEscrow(firstEntity, secondEntity, name, amount) {
    const escrowAcc = await this.createEscrowAccount(name, amount);
    const escrowPair = escrowAcc.getKeyPair();
    this.escrowObject = escrowAcc.getAccountObject();
    try {
      let escrowTransaction = new Stellar.TransactionBuilder(this.escrowObject)
        .addOperation(
          Stellar.Operation.setOptions({
            signer: {
              ed25519PublicKey: firstEntity.getPublicKey(),
              weight: 1
            }
          })
        )
        .addOperation(
          Stellar.Operation.setOptions({
            masterWeight: 0,
            lowThreshold: 2,
            medThreshold: 2,
            highThreshold: 2,
            signer: {
              ed25519PublicKey: secondEntity.getPublicKey(),
              weight: 1
            }
          })
        )
        .build();
      escrowTransaction.sign(escrowPair);
      await Server.submitTransaction(escrowTransaction);
      await firstEntity.sendTransaction(
        100.0000001,
        escrowAcc,
        Stellar.Memo.text(
          `Coti to ${escrowAcc.getName()} from ${firstEntity.getName()}`
        )
      );
      await secondEntity.sendTransaction(
        100.0000001,
        escrowAcc,
        Stellar.Memo.text(
          `Coti to ${escrowAcc.getName()} from ${secondEntity.getName()}`
        )
      );
      return escrowAcc;
    } catch (error) {
      throw error;
    }
  }

  async closeSmartContract(
    firstEntity,
    secondEntity,
    memo = Stellar.Memo.text(`Payment from escrow`)
  ) {
    try {
      const closingTransaction = new Stellar.TransactionBuilder(
        this.escrowObject
      )
        .addOperation(
          Stellar.Operation.payment({
            destination: secondEntity.getPublicKey(),
            asset: Stellar.Asset.native(),
            amount: `500.0000001`
          })
        )
        .addMemo(memo)
        .build();
      closingTransaction.sign(secondEntity.getKeyPair());
      closingTransaction.sign(firstEntity.getKeyPair());
      await Server.submitTransaction(closingTransaction);
      firstEntity.displayBalance();
      secondEntity.displayBalance();
    } catch (error) {
      throw error;
    }
  }
}

module.exports = StellarCore;
