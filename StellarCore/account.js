const Stellar = require("stellar-sdk");
const Request = require("request-promise");
const Config = require("../config/config.js");

class Account {
  constructor(server, name) {
    this.name = name;
    this.server = server;
    this.pair = null;
    this.accountObject = null;
    this.balances = [];
    this.txHistory = [];
    this.init();
  }

  init() {
    this.pair = Stellar.Keypair.random();
    console.log(`${this.name}'s public key : ${this.pair.publicKey()}`);
  }

  async initAccount() {
    try {
      await Request.get({
        uri: `${Config.api_url}/friendbot`,
        qs: { addr: this.pair.publicKey() },
        json: true
      });
      this.accountObject = await this.server.loadAccount(this.pair.publicKey());
    } catch (error) {
      throw error;
    }
    this.balances = this.accountObject.balances;
    this.displayBalance();
  }

  async initEscrow() {
    try {
      await Request.get({
        uri: `${Config.api_url}/friendbot`,
        qs: { addr: this.pair.publicKey() },
        json: true
      });
      this.accountObject = await this.server.loadAccount(this.pair.publicKey());
    } catch (error) {
      throw error;
    }
  }

  async refresh() {
    try {
      this.accountObject = await this.server.loadAccount(this.pair.publicKey());
      this.balances = this.accountObject.balances;
    } catch (error) {
      throw error;
    }
  }

  isAccountInitialized() {
    return this.accountObject !== null;
  }

  getName() {
    return this.name;
  }

  getPublicKey() {
    return this.pair.publicKey();
  }

  getKeyPair() {
    return this.pair;
  }

  getBalances() {
    return this.balances;
  }

  getAccountObject() {
    return this.accountObject;
  }

  displayBalance() {
    console.log(`${this.name}'s balance : ${this.balances[0].balance} XLM`);
  }

  isTransactionDoable(amout) {
    for (let i = 0; i < this.balances.length; i++) {
      if (this.balances[i].balance >= amout) {
        return true;
      }
    }
    return false;
  }

  addTradeToHistory(tradeObject) {
    this.txHistory.push(tradeObject);
  }

  getHistory() {
    return this.txHistory;
  }

  async sendTransaction(
    amount,
    destination,
    memo = Stellar.Memo.text("Test transac")
  ) {
    const timestamp = Date.now();
    let historyPage = null;
    let transacURL = null;
    try {
      const transaction = new Stellar.TransactionBuilder(this.accountObject)
        .addOperation(
          Stellar.Operation.payment({
            destination: destination.getPublicKey(),
            asset: Stellar.Asset.native(),
            amount: `${amount}`
          })
        )
        .addMemo(memo)
        .build();
      transaction.sign(this.pair);

      const transacRes = await this.server.submitTransaction(transaction);
      transacURL = transacRes._links.transaction.href;
      this.addTradeToHistory({
        timestamp,
        sender_name: this.getName(),
        sender_pkey: this.getPublicKey(),
        receiver_name: destination.getName(),
        receiver_pkey: destination.getPublicKey(),
        tx_url: transacURL,
        amount
      });
      destination.addTradeToHistory({
        timestamp,
        sender_name: this.getName(),
        sender_pkey: this.getPublicKey(),
        receiver_name: destination.getName(),
        receiver_pkey: destination.getPublicKey(),
        tx_url: transacURL,
        amount
      });
      await this.refresh();
      await destination.refresh();
    } catch (error) {
      throw error;
    }
    this.displayTransactionResult(amount, destination);
  }

  displayTransactionResult(amount, destination) {
    console.log(
      `${amount} XLM transferred by ${this.getName()} to ${destination.getPublicKey()}`
    );
  }
}

module.exports = Account;
