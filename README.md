# Projet Workshop

## Introduction

Ce projet a pour but de créer un POC utilisant la blockchain et permettant d'automatiser les remboursements de l'assurance AXA.

## Installation

- Installer NodeJS [Tuto](https://openclassrooms.com/fr/courses/1056721-des-applications-ultra-rapides-avec-node-js/1056956-installer-node-js)

- Installer [GIT](https://git-scm.com/book/fr/v1/D%C3%A9marrage-rapide-Installation-de-Git#Installation-sur-Mac)

- Créer une clé SSH et la lier au compte GitLab [Doc git](https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html)

- Créer un répertoire `workflow` et y lancer la commande `git clone git@gitlab.com:Clonescody/workshop.git`

- Lancer la commande `npm install`

- Lancer `node app.js` pour lancer l'API

- Dans un autre terminal, lancer Maildev avec la commande `maildev` 

- Lancer l'interface en se déplacant dans le dossier `./Interface/react-complete-guide/` et en lancant `npm install --save && npm start`

## Config

- `api_url` => URL de l'API Horizon pour accéder à la blockchain Stellar

## Utilisation

L'application utilise le testnet de la blockchain Stellar.

Il est possible de : 

`GET`
- Afficher les assurances d'un utilisateur
- Afficher la balance d'un compte : `http://localhost:5000/account_balance/<acc_name>`
- Afficher l'historique des transactions : `http://localhost:5000/history/<acc_name>`

`POST`

Les URL en POST utilisent toute Content-type : `x-www-form-urlencoded`

- Login un utilisateur et créer son compte dans l'API

- Créer un/plusieurs compte : POST `http://localhost:5000/create_acc`, param: `name`
Chaque compte sera créé avec une balance de 10,000.00 XLM par défaut

- Effectuer une transaction entre 2 comptes : `http://localhost:5000/make_transaction`, params: `sender`, `receiver`, `amount`
- Créer un compte Escrow : `http://localhost:5000/make_escrow`, params: `first`, `second`, `amount`
- Liquider le compte Escrow : `http://localhost:5000/close_contract`, params: `first`, `second`, `amount`

Les paramètres `sender`, `receiver`, `first` et `second` sont les `<acc_name>` utilisés à la création


Pour vérifier le bon fonctionnement, il est possible d'utiliser directement le [Stellar Blockchain Explorer](http://testnet.stellarchain.io/) et de soumettre une des adresse dans le champ de recherche. 

TODO : 
- Terminer la liaison entre l'API et le StellarCore
- Terminer l'interface et afficher les infos du compte (public key, balance)
- Permettre la création et le trigger du compte escrow via une requête POST depuis l'interface