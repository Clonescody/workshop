const nodemailer = require("nodemailer");
const mailercfg = require("../config/maildev.js");

class Mailer {
  constructor(type) {
    this.transporter = null;
    this.mailOptions = null;
    this.type = type;
    this.init();
  }

  init() {
    this.transporter = nodemailer.createTransport({
      host: mailercfg.host,
      port: mailercfg.port,
      ignoreTLS: true
    });
    this.mailOptionsCreate = {
      from: "test.axa@yopmail.com", // sender address
      to: "test.client@yopmail.com", // list of receivers
      subject: "Creation", // Subject line
      text: "Your Stellar wallet is being created", // plain text body
      html: "<b>Your Stellar wallet is being created</b>" // html body
    };
    this.mailOptionsPayment = {
      from: "test.axa@yopmail.com", // sender address
      to: "test.client@yopmail.com", // list of receivers
      subject: "Payment", // Subject line
      text: "Escrow payment done", // plain text body
      html: "<b>Escrow payment done</b>" // html body
    };
  }

  getMailOptions(type, user) {
    this.mailOptionsCreate = {
      from: "test.axa@yopmail.com", // sender address
      to: "test." + user.name + "@yopmail.com", // list of receivers
      subject: "Creation", // Subject line
      text: "Your Stellar wallet is being created", // plain text body
      html: "<b>Your Stellar wallet is being created</b>" // html body
    };
    this.mailOptionsPayment = {
      from: "test.axa@yopmail.com", // sender address
      to: "test." + user.name + "@yopmail.com", // list of receivers
      subject: "Payment", // Subject line
      text: "Escrow payment done", // plain text body
      html: "<b>Escrow payment done</b>" // html body
    };
    return type === "CREATE" ? this.mailOptionsCreate : this.mailOptionsPayment;
  }

  sendMail(type, user) {
    const options = this.getMailOptions(type, user);
    this.transporter.sendMail(options, (error, info) => {
      if (error) {
        return console.log(error);
      }
      console.log("Message sent: %s", info.messageId);
    });
  }
}

module.exports = Mailer;
