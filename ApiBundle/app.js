const express = require("express");
const bodyParser = require("body-parser");
const StellarCore = require("../StellarCore/app.js");
const Mailer = require("../common/mailer.js");
const routes = require("./routes/routes.js");

class APIBundle {
  constructor() {
    this.stellarCore = new StellarCore();
    this.mailer = new Mailer();
  }
  start() {
    let app = express();

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    routes(app, this.stellarCore, this.mailer);

    let server = app.listen(5000, () => {
      console.log(`API running on port ${server.address().port}.`);
    });
  }
}

module.exports = APIBundle;
