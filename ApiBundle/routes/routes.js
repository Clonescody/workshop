const mysql = require("mysql");

const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "workshop",
  database: "workshop",
  insecureAuth: true
});

let appRouter = (app, stellarCore, mailer) => {
  app.get("/", (req, res) => {
    res.status(200).send("Welcome to our restful API");
  });

  app.post("/create_acc", (req, res) => {
    stellarCore.initAccount(req.body.name);
    mailer.sendMail("CREATE", { name: req.body.name });
    res.status(200).send(`Account ${req.body.name} is being created`);
  });

  app.get("/account/:name", (req, res) => {
    if (stellarCore.isAccountInitialized(req.params.name)) {
      const account = stellarCore.getAccountByName(req.params.name);
      const query =
        "UPDATE users SET public_key=" +
        account.public_key +
        'WHERE login="' +
        account.name +
        '";';
      connection.query(query, function(error, result, fields) {
        if (error) {
          throw error;
        }
        console.log("update result : ", result);

        res.status(200).send("Account updated");
      });
      res.status(200).send(account);
    } else {
      res.status(200).send(`Account ${req.params.name} is not ready yet`);
    }
  });

  app.get("/account_balance/:name", (req, res) => {
    if (stellarCore.isAccountInitialized(req.params.name)) {
      const acc = stellarCore.getAccountByName(req.params.name);
      res.status(200).send(acc.getBalances()[0]);
    } else {
      res.status(200).send(`Account ${req.params.name} is not ready yet`);
    }
  });

  app.post("/make_transaction", (req, res) => {
    const senderAcc = stellarCore.getAccountByName(req.body.sender);
    const receiverAcc = stellarCore.getAccountByName(req.body.receiver);
    if (
      stellarCore.isAccountInitialized(req.body.sender) &&
      stellarCore.isAccountInitialized(req.body.receiver)
    ) {
      senderAcc.sendTransaction(req.body.amount, receiverAcc);
      res
        .status(200)
        .send(`${req.body.amount} XLM sent to ${receiverAcc.getName()}`);
    } else {
      if (!stellarCore.isAccountInitialized(req.body.sender)) {
        res.status(200).send(`Account ${req.body.sender} is not ready yet`);
      }
      if (!stellarCore.isAccountInitialized(req.body.receiver)) {
        res.status(200).send(`Account ${req.body.receiver} is not ready yet`);
      }
    }
  });

  app.get("/history/:name", (req, res) => {
    const acc = stellarCore.getAccountByName(req.params.name);
    if (stellarCore.isAccountInitialized(req.params.name)) {
      res.status(200).send(acc.getHistory());
    } else {
      res.status(200).send(`Account ${req.params.name} is not ready yet`);
    }
  });

  app.post("/make_escrow", (req, res) => {
    const firstEntity = stellarCore.getAccountByName(req.body.first);
    const secondEntity = stellarCore.getAccountByName(req.body.second);
    let escrowAcc = null;
    try {
      escrowAcc = stellarCore.createThisShit(
        secondEntity,
        firstEntity,
        "Escrow",
        "250.0000"
      );
    } catch (error) {
      res.status(200).send(`Escrow error ${error}`);
    }
    if (
      stellarCore.isAccountInitialized(req.body.first) &&
      stellarCore.isAccountInitialized(req.body.second) &&
      stellarCore.isAccountInitialized("Escrow")
    ) {
      secondEntity.sendTransaction(secondEntity, req.body.amount, escrowAcc);
      res.status(200).send(`Escrow initialized`);
    } else {
      if (!stellarCore.isAccountInitialized(req.body.first)) {
        res.status(200).send(`Account ${req.body.first} is not ready yet`);
      }
      if (!stellarCore.isAccountInitialized(req.body.second)) {
        res.status(200).send(`Account ${req.body.second} is not ready yet`);
      }
      if (!stellarCore.isAccountInitialized("Escrow")) {
        res.status(200).send(`Escrow account is being initialized`);
      }
    }
  });

  app.post("/close_contract", (req, res) => {
    const firstEntity = stellarCore.getAccountByName(req.body.first);
    const secondEntity = stellarCore.getAccountByName(req.body.second);
    if (
      stellarCore.isAccountInitialized(req.body.first) &&
      stellarCore.isAccountInitialized(req.body.second) &&
      stellarCore.isAccountInitialized("Escrow")
    ) {
      stellarCore.closeSmartContract(firstEntity, secondEntity);

      res.status(200).send(`Escrow liquidation`);
    } else {
      if (!stellarCore.isAccountInitialized(req.body.first)) {
        res.status(200).send(`Account ${req.body.first} is not ready yet`);
      }
      if (!stellarCore.isAccountInitialized(req.body.second)) {
        res.status(200).send(`Account ${req.body.second} is not ready yet`);
      }
      if (!stellarCore.isAccountInitialized("Escrow")) {
        res.status(200).send(`Escrow account is not ready`);
      }
    }
  });

  app.post("/login", (req, res) => {
    connection.query(`SELECT * FROM users;`, function(error, results, fields) {
      if (error) {
        throw error;
      }
      let user = null;
      for (let i = 0; i < results.length; i++) {
        if (
          results[i].login === req.body.login &&
          results[i].password === req.body.password
        ) {
          const userReturned = results[i];
          user = { ...userReturned, isLoggedIn: true };
          break;
        }
      }

      console.log("user returned ; ", user);
      if (user !== null) {
        stellarCore.initAccount(req.body.login);
        mailer.sendMail("CREATE", user);
        res.status(200).send(user);
      } else {
        console.log("error");
      }
    });
  });

  app.post("/insurances", (req, res) => {
    connection.query(`SELECT * FROM insurances;`, function(
      error,
      result,
      fields
    ) {
      if (error) {
        throw error;
      }
      console.log("results : ", result[0]);
      if (error) {
        throw error;
      }
      const insurance = { ...result[0] };

      console.log("insurance returned ; ", insurance);
      if (insurance !== null) {
        res.status(200).send(insurance);
      } else {
        console.log("error");
      }
    });
  });

  app.post("/create_insurances", (req, res) => {
    const query =
      "INSERT INTO insurances(name, description, amount, id_user) VALUES(" +
      req.body.name +
      "," +
      req.body.description +
      "," +
      req.body.amount +
      "," +
      req.body.id_user +
      ")";
    connection.query(query, function(error, result, fields) {
      if (error) {
        throw error;
      }
      console.log("create result : ", result);
      if (error) {
        throw error;
      }
      res.status(200).send("Being created");
    });
  });
};

module.exports = appRouter;
