import React from "react";
import Form from "../partials/loginForm.js";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      error: null
    };
    this.loginAndNavRef = this.loginAndNav.bind(this);
  }

  loginAndNav(user, error) {
    this.setState({ user, error });
    console.log("callback", user);
  }

  render() {
    console.log("state : ", this.state);
    return (
      <div id="root" className="App">
        <Form callback={this.loginAndNavRef} />
      </div>
    );
  }
}

export default Login;
