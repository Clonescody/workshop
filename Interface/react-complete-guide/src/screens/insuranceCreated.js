import React from "react";
import { Link } from "react-router-dom";

class InsuranceCreated extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      insurance: null,
      error: null
    };
  }

  componentDidMount() {
    let formBody = [];
    const userProps = {
      login: this.props.location.state.login,
      password: this.props.location.state.password
    };
    for (let property in userProps) {
      let encodedKey = encodeURIComponent(property);
      let encodedValue = encodeURIComponent(userProps[property]);
      formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");

    fetch("http://localhost:5000/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
      },
      body: formBody
    })
      .then(resp => resp.json())
      .then(user => this.setState({ user }))
      .catch(error => this.setState({ error }));
  }

  dispatchUser() {
    console.log("state : ", this.props.location.state);
    if (this.props.location.state.dispatchCallback) {
      this.props.location.state.dispatchCallback(this.state.user);
    }
  }

  render() {
    console.log("homepage", this.state.user);
    return this.state.user === null ? (
      <div>
        <p>{"Chargement..."}</p>
      </div>
    ) : (
      <div>
        <p>{`Vous etes connecté en tant que ${this.state.user.name} ${
          this.state.user.firstname
        }`}</p>
        <div>
          <p>
            {"Voir vos assurances : "}
            <Link to={{ pathname: "/insurances", user: this.state.user }}>
              {"Mes assurances"}
            </Link>
          </p>
        </div>
      </div>
    );
  }
}

export default InsuranceCreated;
