import React from "react";
import { Link } from "react-router-dom";
class Insurances extends React.Component {
  constructor() {
    super();
    this.state = {
      insurances: null
    };
  }
  componentDidMount() {
    let formBody = [];
    const userProps = {
      user_id: this.props.location.user.id
    };
    for (let property in userProps) {
      let encodedKey = encodeURIComponent(property);
      let encodedValue = encodeURIComponent(userProps[property]);
      formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");

    fetch("http://localhost:5000/insurances", {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
      },
      body: formBody
    })
      .then(resp => resp.json())
      .then(insurances =>
        this.setState({
          insurances: typeof insurances === Array ? insurances : [insurances],
          user: this.props.location.user
        })
      )
      .catch(error =>
        this.setState({ error: error, user: this.props.location.user })
      );
  }
  render() {
    console.log("state.insurances :", this.state.insurances);
    return this.state.insurances === null ? (
      <div>
        <p>tetetetetetetetetetete</p>
      </div>
    ) : (
      <div>
        <div>
          {this.state.insurances.map(function(insurance) {
            return (
              <div key={insurance.id}>
                <p>{insurance.name}</p>
                <p>{insurance.description}</p>
              </div>
            );
          })}
        </div>

        <div>
          <Link to={{ pathname: "/create_insurance", user: this.state.user }}>
            {"Souscrire à une autre assurance"}
          </Link>
        </div>
      </div>
    );
  }
}

export default Insurances;
