import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Login from "./screens/login.js";
import Insurances from "./screens/insurances.js";
import Homepage from "./screens/homepage.js";
import CreateInsurance from "./partials/createInsurance.js";
import "./App.css";

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      user: null
    };
    this.dispatchUserRef = this.dispatchUser.bind(this);
    this.createInsurerAccount();
  }

  createInsurerAccount() {
    let formBody = [];
    const userProps = {
      name: "AXA"
    };
    for (let property in userProps) {
      let encodedKey = encodeURIComponent(property);
      let encodedValue = encodeURIComponent(userProps[property]);
      formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");
    fetch("http://localhost:5000/create_acc", {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
      },
      body: formBody
    });
  }

  dispatchUser(user) {
    console.log("user dispatched", user);
    this.setState({ user });
  }

  render() {
    return (
      <Router>
        <div>
          <nav>
            <ul>
              <li>
                <Link to="/">Login</Link>
              </li>
              <li>
                <Link to="/homepage">Homepage</Link>
              </li>
              <li>
                <Link to="/insurances">Assurances</Link>
              </li>
            </ul>
          </nav>
          <div id="root">
            {this.state.user === null ? (
              <Route path="/" exact component={Login} />
            ) : null}
            <Route
              path="/homepage/"
              component={Homepage}
              state={{
                user: this.state.user,
                dispatchCallback: this.dispatchUserRef
              }}
            />
            <Route
              path="/insurances/"
              component={Insurances}
              state={this.state.user}
            />
            <Route
              path="/create_insurance/"
              component={CreateInsurance}
              state={this.state.user}
            />
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
