import React from "react";
import { Redirect } from "react-router-dom";

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      login: "",
      password: "",
      error: null,
      submitted: false
    };

    this.handleLoginChange = this.handleLoginChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleLoginChange(event) {
    this.setState({ login: event.target.value });
  }

  handlePasswordChange(event) {
    this.setState({ password: event.target.value });
  }

  async handleSubmit() {
    console.log("handle");
    this.setState({
      submitted: true,
      user: {
        login: this.state.login,
        password: this.state.password
      }
    });
  }

  render() {
    return this.state.submitted ? (
      <Redirect to={{ pathname: "/homepage/", state: this.state.user }} />
    ) : (
      <form onSubmit={this.handleSubmit}>
        <label>
          Login:
          <input
            type="text"
            value={this.state.login}
            onChange={this.handleLoginChange}
          />
        </label>
        <label>
          Password:
          <input
            type="password"
            value={this.state.password}
            onChange={this.handlePasswordChange}
          />
        </label>
        <input type="submit" value="Submit" />
      </form>
    );
  }
}

export default Form;
