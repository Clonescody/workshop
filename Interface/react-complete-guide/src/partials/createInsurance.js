import React from "react";
import { Redirect } from "react-router-dom";

class CreateInsuranceForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: props.location.user,
      insurance: null,
      user_id: props.location.user.id,
      name: "",
      description: "",
      amount: 150.0,
      error: null,
      submitted: false
    };

    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
    this.handleAmountChange = this.handleAmountChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleNameChange(event) {
    this.setState({ name: event.target.value });
  }

  handleDescriptionChange(event) {
    this.setState({ description: event.target.value });
  }

  handleAmountChange(event) {
    this.setState({ amount: event.target.value });
  }

  async handleSubmit() {
    console.log("handle");
    this.setState({
      submitted: true,
      insurance: {
        name: this.state.name,
        description: this.state.description,
        amount: this.state.amount
      }
    });
  }

  render() {
    return this.state.submitted ? (
      <Redirect
        to={{
          pathname: "/insurance_created/",
          state: { user: this.state.user, insurance: this.state.insurance }
        }}
      />
    ) : (
      <form onSubmit={this.handleSubmit}>
        <label>
          <p>{this.state.name}</p>
          <input
            type="text"
            value={this.state.name}
            onChange={this.handleNameChange}
            hidden
          />
        </label>
        <label>
          {this.state.description}
          <input
            type="text"
            value={this.state.description}
            onChange={this.handleDescriptionChange}
            hidden
          />
        </label>
        <p>{`Prix : ${this.state.amount} XLM`}</p>
        <input type="number" value={this.state.amount} hidden />
        <input type="number" value={this.state.user_id} hidden />
        <input type="submit" value="Souscrire" />
      </form>
    );
  }
}

export default CreateInsuranceForm;
